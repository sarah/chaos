package chaos

type Template struct {
	Structures map[string]*GSet
	// eg. 2P-Set "add": "add", "remove":"remove"
	Commands    map[string][]string
	LookupIn    []string
	LookupNotIn []string
}

func NewTemplate() *Template {
	return &Template{Structures: make(map[string]*GSet), Commands: make(map[string][]string)}
}

func (t *Template) InitStructures(structures ...string) {
	for _, structure := range structures {
		t.Structures[structure] = NewGSet()
	}
}

func (t *Template) NewCommand(cmd string, structures ...string) {
	t.Commands[cmd] = structures
}

func (t *Template) ExecCmd(cmd string, element string) {
	for _, structure := range t.Commands[cmd] {
		t.Structures[structure].Add(element)
	}
}

func (t *Template) Len(structure string) int{
	if _,exists :=  t.Structures[structure]; exists {
		return t.Structures[structure].Len()
	}
	return 0
}

func (t *Template) Lookup(element string) bool {
	for _, structure := range t.LookupIn {
		if t.Structures[structure].Lookup(element) == false {
			return false
		}
	}

	for _, structure := range t.LookupNotIn {
		if t.Structures[structure].Lookup(element) == true {
			return false
		}
	}
	return true
}
