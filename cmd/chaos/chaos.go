package main

import (
	"encoding/json"
	"fmt"
	"git.openprivacy.ca/sarah/chaos"
	"io/ioutil"
	"os"
	"path"
)

func cmd_new(args []string) {
	if len(args) == 2 {
		template_path := path.Join(args[0] + ".cst")
		if _, err := os.Stat(template_path); err == nil {
			data, _ := ioutil.ReadFile(template_path)
			template := chaos.NewTemplate()
			err := json.Unmarshal(data, template)
			if err == nil {
				odata, _ := json.Marshal(template)
				tpath := path.Join(".", ".chaos", args[1])
				ioutil.WriteFile(tpath, odata, 0644)
			} else {
				fmt.Printf("Could not parse template for %v", args[0])
			}
		} else {
			fmt.Printf("No template found for %v", args[0])
		}
	}

}

func cmd(cmd string, args []string) {
	path := path.Join(".", ".chaos", args[0])
	if _, err := os.Stat(path); err == nil {
		data, _ := ioutil.ReadFile(path)
		template := chaos.NewTemplate()
		err := json.Unmarshal(data, template)
		if err != nil {
			fmt.Printf("could not execute \"%v\" on %v", cmd, args[0])
		}
		template.ExecCmd(cmd, args[1])
		json, _ := json.Marshal(template)
		ioutil.WriteFile(path, json, 0644)
	} else {
		fmt.Printf("Chaos structure \"%v\" does not exist\n", args[1])
		return
	}
}

func cmd_lookup(args []string) {
	if len(args) == 2 {
		path := path.Join(".", ".chaos", args[0])
		if _, err := os.Stat(path); err == nil {
			data, _ := ioutil.ReadFile(path)
			template := chaos.NewTemplate()
			err := json.Unmarshal(data, template)
			if err != nil {
				fmt.Printf("Cannot parse \"%v\"", args[0])
				return
			}
			fmt.Printf("%v\n", template.Lookup(args[1]))
		} else {
			fmt.Printf("Chaos structure \"%v\" does not exist\n", args[0])
			return
		}
	} else {
		fmt.Printf("Usage: chaos lookup <structure> <key>\n")
	}
}

func cmd_len(args []string) {
	if len(args) == 2 {
		path := path.Join(".", ".chaos", args[0])
		if _, err := os.Stat(path); err == nil {
			data, _ := ioutil.ReadFile(path)
			template := chaos.NewTemplate()
			err := json.Unmarshal(data, template)
			if err != nil {
				fmt.Printf("Cannot parse \"%v\"", args[0])
				return
			}
			fmt.Printf("%v\n", template.Len(args[1]))
		} else {
			fmt.Printf("Chaos structure \"%v\" does not exist\n", args[0])
			return
		}
	} else {
		fmt.Printf("Usage: chaos lookup <structure> <key>\n")
	}
}

func main() {
	if len(os.Args) >= 2 {
		switch os.Args[1] {
		case "init":
			os.Mkdir("./.chaos", 0755)
		case "new":
			cmd_new(os.Args[2:])
		case "lookup":
			cmd_lookup(os.Args[2:])
		case "len":
			cmd_len(os.Args[2:])
		default:
			cmd(os.Args[1], os.Args[2:])
		}
		os.Exit(0)
	}
	os.Exit(1)
}
