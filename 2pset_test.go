package chaos

import "testing"

func TestNewTPSet(t *testing.T) {
	a := NewTPSet()
	b := NewTPSet()

	a.Add("1")
	a.Add("2")
	a.Add("3")
	a.Add("4")
	b.Merge(a)

	a.Remove("2")
	b.Remove("3")
	a.Merge(b)
	b.Merge(a)

	if a.Compare(b) == false {
		t.Fatalf("a and b should be the same: a:%v b:%v", a, b)
	}

	if a.Lookup("3") != false || b.Lookup("3") != false {
		t.Fatalf("3 should not be in a %v or b %v", a.Lookup("3"), b.Lookup("3"))
	}

	t.Logf("a %v", a)
	t.Logf("b %v", b)
}
