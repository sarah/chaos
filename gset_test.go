package chaos

import "testing"

func TestNewGSet(t *testing.T) {
	a := NewGSet()
	b := NewGSet()

	a.Add("1")
	a.Add("2")
	a.Add("3")
	a.Add("4")

	a.Add("1")
	a.Add("2")
	a.Add("3")
	a.Add("4")

	b.Add("1")
	b.Add("2")
	b.Add("3")
	b.Add("4")

	a.Add("5")

	if b.Compare(a) == false {
		t.Fatalf("b %v  should be a subset of a %v", a, b)
	}

	t.Logf("a: %v", a)
	t.Logf("b: %v", b)

	b.Merge(a)
	a.Merge(b)

	if b.Compare(a) == false {
		t.Fatalf("b %v should be a subset of a %v", a, b)
	}

	t.Logf("a: %v", a)
	t.Logf("b: %v", b)

}
