package chaos

type GSet struct {
	Elements map[string]bool `json: "Elements"`
}

func NewGSet() *GSet {
	return &GSet{Elements: make(map[string]bool)}
}

func (gs *GSet) Add(element string) {
	gs.Elements[element] = true
}

func (gs *GSet) Len() int {
	return len(gs.Elements)
}

func (gs *GSet) Lookup(element string) bool {
	_, exists := gs.Elements[element]
	return exists
}

func (gs *GSet) Compare(gset *GSet) bool {
	for k := range gs.Elements {
		hasK := gset.Lookup(k)
		if !hasK {
			return false
		}
	}
	return true
}

func (gs *GSet) Merge(gset *GSet) *GSet {
	for k := range gset.Elements {
		gs.Add(k)
	}
	return gs
}
