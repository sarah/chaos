package chaos

import "fmt"

type TPSet struct {
	add    *GSet
	remove *GSet
}

func NewTPSet() *TPSet {
	return &TPSet{add: NewGSet(), remove: NewGSet()}
}

func (tpset TPSet) Lookup(element string) bool {
	return tpset.add.Lookup(element) && !tpset.remove.Lookup(element)
}

func (tpset *TPSet) Add(element string) {
	tpset.add.Add(element)
}

func (tpset *TPSet) Remove(element string) {
	tpset.remove.Add(element)
}

func (tpset TPSet) Compare(set *TPSet) bool {
	return tpset.add.Compare(set.add) && tpset.remove.Compare(set.remove)
}

func (tpset *TPSet) Merge(set *TPSet) *TPSet {
	ret := new(TPSet)
	ret.add = tpset.add.Merge(set.add)
	ret.remove = tpset.remove.Merge(set.remove)
	return ret
}

func (tpset TPSet) String() string {
	return fmt.Sprintf("add: %v, remove: %v", tpset.add, tpset.remove)
}
