package chaos

import (
	"encoding/json"
	"testing"
)

func TestNewTemplate(t *testing.T) {
	template := NewTemplate()
	template.InitStructures("add", "remove")
	template.NewCommand("add", "add")
	template.NewCommand("remove", "remove")
	template.LookupIn = []string{"add"}
	template.LookupNotIn = []string{"remove"}

	data, _ := json.Marshal(template)
	t.Logf("Template: %s\n", data)

	template.ExecCmd("add", "1")
	if template.Lookup("1") == false {
		t.Fatalf("Looking up 1 should have succeeded %v", template)
	}
	template.ExecCmd("remove", "1")
	if template.Lookup("1") {
		t.Fatalf("Looking up 1 should have failed")
	}
}
