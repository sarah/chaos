# Chaos - Distributed Structures

Chaos is a framework for building distributed conflict-free data structures by composing G-Sets (Grow Only Sets)

From these simple sets, more complicated objects can be made e.g. we can build a 2-Phase Set that allows us to remove elements with a simple template (`2pset.cst`)

    {
        "Structures":{
            "add":{"Elements":{}},
            "remove":{"Elements":{}}
        },
        "Commands":{
            "add":["add"],"remove":["remove"]
        },
        "LookupIn":["add"],
        "LookupNotIn":["remove"]
    }

We can also build distributed counters, e.g. a counter to synchronize 4 nodes might look like this ([`4count.cst`](4count.cst):

    {
        "Structures":{
            "1":{"Elements":{}},
            "2":{"Elements":{}},
            "3":{"Elements":{}},
            "4":{"Elements":{}}
        },
        "Commands":{
            "add1":["1"],
            "add2":["2"],
            "add3":["3"],
            "add4":["4"]
        },
        "LookupIn": ["1","2","3","4"],
        "LookupNotIn":[]
     }

We can then use `chaos`:

    chaos new 4count counter
    # Receive from 1
    chaos add1 counter 1
    # Receive from 2
    chaos add2 counter 1
    # Receive from 3
    chaos add3 counter 1
    # Receive from 4
    chaos add4 counter 1
    # Get Max Counter Value
    seq 4 | xargs -I{} chaos len counter | sort -nr | head -1
    1 
    # Receive from 4
    chaos add4 counter "in reality this would be update event 2"
    # Get Max Counter Value
    seq 4 | xargs -I{} chaos len counter | sort -nr | head -1
    2 