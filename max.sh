#!/bin/sh
# Find the Max Counter Value in an n-Count structure
seq $2 | xargs -I{} chaos len $1 {} | sort -nr | head -1